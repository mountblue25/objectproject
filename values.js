let result = []

function value(object){
    for (let key in object){
        if (typeof(object[key]) !== "function"){
            result.push(object[key])
        }
    }
    return result
}

module.exports = value;
