
function defaults(object, defaultObject){
    for (let key in defaultObject){
        if (key in object){
            continue;
        } else{
            object[key] = defaultObject[key]
        }
    }
    return object;
}

module.exports = defaults;