let result = {};

function invert(object) {
    for (let key in object){
        result[object[key]] = key;
    }
    return result;
}

module.exports = invert;